#**Traffic Sign Recognition** 

---

**Build a Traffic Sign Recognition Project**

The goals / steps of this project are the following:
* Load the data set (see below for links to the project data set)
* Explore, summarize and visualize the data set
* Design, train and test a model architecture
* Use the model to make predictions on new images
* Analyze the softmax probabilities of the new images
* Summarize the results with a written report

## Rubric Points
###Here I will consider the [rubric points](https://review.udacity.com/#!/rubrics/481/view) individually and describe how I addressed each point in my implementation.  

---
###Writeup / README

####1. Provide a Writeup / README that includes all the rubric points and how you addressed each one. You can submit your writeup as markdown or pdf. You can use this template as a guide for writing the report. The submission includes the project code.

You're reading it! and here is a link to my [project code](https://bitbucket.org/hgw2101/carnd-traffic-sign-classifier-project)

###Data Set Summary & Exploration

####1. Provide a basic summary of the data set and identify where in your code the summary was done. In the code, the analysis should be done using python, numpy and/or pandas methods rather than hardcoding results manually.

The code for this step is contained in the 8th code cell of the IPython notebook.

I used the numpy library and Python's native list methods (i.e len()) to calculate summary statistics of the traffic
signs data set:

* The size of training set is 34799
* The size of validation set is 4410
* The size of test set is 12630
* The shape of a traffic sign image is (32, 32, 3)
* The number of unique classes/labels in the data set is 43

####2. Include an exploratory visualization of the dataset and identify where the code is in your code file.

The code for this step is contained in the 10th code cell of the IPython notebook.  

###Design and Test a Model Architecture

####1. Describe how, and identify where in your code, you preprocessed the image data. What tecniques were chosen and why did you choose these techniques? Consider including images showing the output of each preprocessing technique. Pre-processing refers to techniques such as converting to grayscale, normalization, etc.

The code for this step is contained in the 2nd, 3rd, 4th and 5th code cells of the IPython notebook.

As a first step, probably like many students, I started out using the code from the LeNet lab. Initially I used my own CPU, it took an average 4 minutes, and the accuracy peaked at only **89%**, so I knew I had to do something different.

####Converting images to grayscale
I converted the images to grayscale by averaging the RGB color channels for each pixel in each image. The reason I decided to do this was that I didn't notice any distinct color patterns that would uniquely identify a particular traffic sign over another, rather it's the shapes of the traffic signs that identify what they are. So, I thought that grayscale would simplify the neural network and let it focus only on things, i.e. shapes of the traffic signs, other than color to classify them.

This took a lot of time and computing power, but the results barely changed, accuracy was still at **~93.5%**. The RGB to grayscale conversion code is in the second code cell of the notebook.

####Other preprocessing steps using OpenCV
As suggested by my project reviewer, I decided to experiment with scaling, translation and rotation using the OpenCV library. Unfortunately none of these helped me improve the accuracy on my validation set. For **scaling**, I tried to double the width and height of the image, but this ended up using too much memory and disk space so I couldn't run my model. For **translation**, the accuracy on the validation set turned out to be very low (in the mid .20s). While for **rotation**, the accuracy peaked at **0.913**. I think if I could have improved the accuracy if I fine tuned the LeNet hyperparameters, but since I was satisfied with the accuracy without preprocessing, I decided not to apply these preprocessing steps.

####2. Describe how, and identify where in your code, you set up training, validation and testing data. How much data was in each set? Explain what techniques were used to split the data into these sets. (OPTIONAL: As described in the "Stand Out Suggestions" part of the rubric, if you generated additional data for training, describe why you decided to generate additional data, how you generated the data, identify where in your code, and provide example images of the additional data)

The code for splitting the data into training and validation sets is contained in the 8th code cell of the IPython notebook.  

Since the raw dataset comes with a validation data set, I did not use any methodology to separate the training data into training and validation data. If I had to do that I would have used `sklearn`'s `shuffle` API.

My final training set had 34799 number of images. My validation set and test set had 4410 and 12630 number of images.

####3. Describe, and identify where in your code, what your final model architecture looks like including model type, layers, layer sizes, connectivity, etc.) Consider including a diagram and/or table describing the final model.

The code for my final model is located in the 14th cell of the ipython notebook. 

My final model consisted of the following layers:

| Layer                 |     Description                               | 
|:---------------------:|:---------------------------------------------:| 
| Input                 | 32x32x3 RGB image                             | 
| Convolution 5x5       | 1x1 stride, same padding, outputs 32x32x12    |
| RELU                  |                                               |
| Max pooling           | 2x2 stride,  outputs 16x16x12                 |
| Convolution 5x5       | 1x1 stride, valid padding, outputs 12x12x22   |
| RELU                  |                                               |
| Dropout               |                                               |
| Max pooling           | 2x2 stride,  outputs 6x6x22                   |
| Convolution 3x3       | 1x1 stride, outputs 4x4x50                    |
| RELU                  |                                               |
| Dropout               |                                               |
| Fully connected       | outputs 1x800 then 1x120 then 1x84 then 1x43  |
| Softmax               |                                               |


####4. Describe how, and identify where in your code, you trained your model. The discussion can include the type of optimizer, the batch size, number of epochs and any hyperparameters such as learning rate.

The code for training the model is located in the 16th cell of the ipython notebook. 

To train the model, I used an cross entropy method with one hot encoding to compare the model outputs with the labels, and then used the AdamOptimizer to optimize/reduce the loss. 

####5. Describe the approach taken for finding a solution. Include in the discussion the results on the training, validation and test sets and where in the code these were calculated. Your approach may have been an iterative process, in which case, outline the steps you took to get to the final solution and why you chose those steps. Perhaps your solution involved an already well known implementation or architecture. In this case, discuss why you think the architecture is suitable for the current problem.

I explored many different ways to improve my model's accuracy. Some of those ideas, e.g. applying dropout and adding more CNN layers, were based on my theoretical understanding of CNNs, but most of these were just trial and error, I didn't want to leave anything off the table and tried as many ways as I could to improve accuracy.

here is what I did sequentially and how they changed the accuracy:

##### 1) Changing sigma to 0.05, i.e. decreasing sigma by a half
This helped accuracy improve a little bit, it went up to **93.7%** by the 10th epoch.

##### 2) Preprocessing color images to grayscale
I converted the images to grayscale by averaging the RGB color channels for each pixel in each image. This took a lot of time and computing power, but the results barely changed, accuracy was still at **~93.5%**.

##### 3) Changing epoch from 10 to 20
This made the training process taking twice the amount of time, but accuracy did not improve much. Nevertheless, I decided to continue using `EPOCH=20` for all my subsequent configuration changes

##### 4) Adding a 1x1 CNN layer
I added this 1x1 CNN layer just after the first CNN layer. My hope was that if the result improved, then I would add more 1x1 CNN layers and make my model follow the inception module. Unfortunately the 1x1 CNN layer did not help, my accuracy dropped slightly to **92.7%**, so I did not add any more 1x1 CNN layer.

##### 5) Switching back to RGB
Up to this point, I'm still using the grayscale images preprocessed in step 2), I decided to switch back to RGB. As expected, this didn't change the accuracy much, which stayed at around **92.4%**.

##### 6) Decreasing batch size from 128 to 100
Since batch size may have some influence on the model, I decided to give it a shot. This took longer to train but the accuracy did improve a little, it reached **95.2%** at epoch 20.

##### 7) Applying a dropout/keep probability of 0.5
To apply dropout, I added a new `keep_prob` parameter to the `LeNet` function, so it became `LeNet(x, keep_prob)`. I applied the dropout at every CNN layer and the final fully connected layer. This unfortunately *decreased* the model's accuracy, to only **91.5%**. (I did make sure to use `keep_prob=1.0` for validation).

##### 8) Resetting batch size and removing 1x1 CNN layer
I decided to try tuning different parameters, so I reset the batch size and removed the 1x1 CNN layer. My accuracy went down to **89.6%**.

##### 9) Only apply dropout on the first CNN layer
Accuracy improved a little, to **93.3%**

##### 10) Reducing the learning rate from `0.001` to `0.0005`
Accuracy remained the same :(

##### 11) Increasing `keep_prob` to `.75` from `.5`
Accuracy again remained the same.

##### 12) Increasing depth layer of CNNs
This improved accuracy quite a lot, it went up to **95.0%**, I was quite happy with it at this point, but wanted to try a few more things.

##### 13) Apply dropout on the first two layers of CNNs
This again helped a bit, accuracy went up to **95.9%**.

##### 14) Apply `SAME` padding on the first CNN layer
This unfortunately decreased the accuracy slightly, down to **94.3%**, since the decrease is small, I decided to keep the `SAME` padding configuration.

##### 15) Apply dropout for all layers of CNNs
This gave me the best result yet :), accuracy improved to **96.1%**.

##### 16) Adding another CNN layer at the end of the second CNN layer, just before the fully connected layer
So now there are a total of 3 CNN layers. The last CNN layer took an input of size `6x6x22` to `4x4x50` and I did not apply the pooling layer at the end of the third CNN layer, because the height/width was already quite small at that point.

Since height and width were further reduced with the newly added CNN layer, I was initially not sure how much the depth should increase by, but later I found out that if the increase in depth is too small, then the parameters used in the fully connected layer will decrease, we don't want that so I made sure that the increase in depth either also increased the overall number of parameters or kept them the same.

This helped improve the accuracy to **96.8%**.

##### Running data against test set

Since I cannot use the test set multiple times, I decided to only use about 1/4 (3000 out of 12630) of the test set to test my model. I was quite happy that the accuracy on the 3000 test data set was **95.1%**.
 

###Test a Model on New Images

####1. Choose five German traffic signs found on the web and provide them in the report. For each image, discuss what quality or qualities might be difficult to classify.

I decided to get 11 German traffic signs on the web. Later I found out that 2 out of the 11 are not included in the original dataset (i.e. the original dataset did not have such signs), so I removed them. Here they are the remaining 9:

![1.jpg](https://bytebucket.org/hgw2101/carnd-traffic-sign-classifier-project/raw/292c7a23377f021193eeb4a0c155cade8be197b7/test_images_found_online/1.jpg) ![4.jpg](https://bytebucket.org/hgw2101/carnd-traffic-sign-classifier-project/raw/292c7a23377f021193eeb4a0c155cade8be197b7/test_images_found_online/4.jpg) ![5.jpg](https://bytebucket.org/hgw2101/carnd-traffic-sign-classifier-project/raw/292c7a23377f021193eeb4a0c155cade8be197b7/test_images_found_online/5.jpg) ![6.jpg](https://bytebucket.org/hgw2101/carnd-traffic-sign-classifier-project/raw/292c7a23377f021193eeb4a0c155cade8be197b7/test_images_found_online/6.jpg) ![7.jpg](https://bytebucket.org/hgw2101/carnd-traffic-sign-classifier-project/raw/292c7a23377f021193eeb4a0c155cade8be197b7/test_images_found_online/7.jpg)![8.jpg](https://bytebucket.org/hgw2101/carnd-traffic-sign-classifier-project/raw/292c7a23377f021193eeb4a0c155cade8be197b7/test_images_found_online/8.jpg)![9.jpg](https://bytebucket.org/hgw2101/carnd-traffic-sign-classifier-project/raw/292c7a23377f021193eeb4a0c155cade8be197b7/test_images_found_online/9.jpg)![10.jpg](https://bytebucket.org/hgw2101/carnd-traffic-sign-classifier-project/raw/292c7a23377f021193eeb4a0c155cade8be197b7/test_images_found_online/10.jpg)![11.jpg](https://bytebucket.org/hgw2101/carnd-traffic-sign-classifier-project/raw/292c7a23377f021193eeb4a0c155cade8be197b7/test_images_found_online/11.jpg)

Here are the potential qualities of these images that might be difficult to classify:

* **Image 1**: This image is a little dark, but there's good contrast between the sign and the background and the background is clear so it shouldn't cause any difficulties
* **Image 2**: This image has a lot of things in the background which might cause the model to mis-classify. The dark traffic light in the background may especially cause issues because most of the traffic signs are black in the middle
* **Image 3**: There is a tree in the background, which might cause some issues. Also this particular traffic sign is more complex than most traffic signs, e.g. #18 general caution, so it may confuse the model
* **Image 4**: There is a face on the right side of the image that covers lower right cornor of the sign, so this may cause the model to mis-classify. However, the content of the sign, i.e. #18 general caution, is clear so it may be okay after all
* **Image 5**: Similar to Image 2, there are lots of things in the background that may cause problems
* **Image 6**: This image has an empty traffic sign on top of the main traffic sign
* **Image 7**: This image should be very easy to classify, since there is nothing in the background, and the the lighting is clear
* **Image 8**: There a few things in the background of this image that may cause problems but the traffic sign takes up most of the image, so it shouldn't be difficult to classify
* **Image 9**: This image should be very easy to classify, since there is nothing in the background, and the the lighting is clear

####2. Discuss the model's predictions on these new traffic signs and compare the results to predicting on the test set. Identify where in your code predictions were made. At a minimum, discuss what the predictions were, the accuracy on these new predictions, and compare the accuracy to the accuracy on the test set (OPTIONAL: Discuss the results in more detail as described in the "Stand Out Suggestions" part of the rubric).

The code for making predictions on my final model is located in the eighteenth cell of the Ipython notebook.

Here are the results of the prediction:

| Image                 |     Prediction                                |  
|:---------------------:|:---------------------------------------------:| 
| Right-of-way at the next intersection| Right-of-way at the next intersection|
| General caution          | General caution                    |
| Roadwork         | Roadwork                     |
| General caution            | General caution                  |
| No Entry     | No Entry                   |
| Bicycle Crossing     | Bicycle Crossing                   |
| 60 km/h     | 60 km/h                   |
| Stop     | Stop                   |
| Roundabout mandatory | Roundabout mandatory |

As I mentioned above, initially my model got 9 of 11 correct. However, the 2 that it did not correctly predict were signs that the training dataset didn't have a label for. So if I removed those, the accuracy reached 100% :).

On the test set, my model got an accuracy of **.942** (this time using all of the test data set, not just 1/3 of it), which I was quite happy about as it appears that my model generalizes well. The accuracy on the images that I found on the internet, if you exclude the 2 mentioned above, is higher than the accuracy of the test set. This does not make sense intuitively, perhaps the images I found online were too easy to classify.


####3. Describe how certain the model is when predicting on each of the five new images by looking at the softmax probabilities for each prediction and identify where in your code softmax probabilities were outputted. Provide the top 5 softmax probabilities for each image along with the sign type of each probability. (OPTIONAL: as described in the "Stand Out Suggestions" part of the rubric, visualizations can also be provided such as bar charts)

The code for making predictions on my final model is located in the 14th cell of the Ipython notebook.

For the first image, the model is relatively sure that this is a stop sign (probability of 0.6), and the image does contain a stop sign. The top five soft max probabilities were

| Probability           |     Prediction                    | 
|:---------------------:|:---------------------------------------------:| 
| .9999999               | Right-of-way at the next intersection | 
| .00000011             | Beware of ice/snow |
| .00000000002         | Pedestrians |
| .000000000002             | Double curve             |
| .00000000000001           | Children crossing |

For the second image ... 

| Probability           |     Prediction                    | 
|:---------------------:|:---------------------------------------------:| 
| .872               | General caution | 
| .127             | Children crossing |
| .00035         | Traffic signals |
| .00013             | Bicycles crossing |
| .00005           | End of all speed and passing limits |

For the third image ... 

| Probability           |     Prediction                    | 
|:---------------------:|:---------------------------------------------:| 
| 1.0               | Road work | 
| .000000003             | Bumpy road |
| .0000000000001         | Bicycles crossing |
| .00000000000004             | Road narrows on the right |
| .000000000000001           | Wild animals crossing |

For the fourth image ... 

| Probability           |     Prediction                    | 
|:---------------------:|:---------------------------------------------:| 
| 1.0               | General caution | 
| .0000000003             | Traffic signals |
| .0000000000001         | Pedestrians |
| .000000000000002             | Road narrows on the right |
| .00000000000000000004           | Speed limit (30km/h) |

And for the fifth image ... 

| Probability           |     Prediction                    | 
|:---------------------:|:---------------------------------------------:| 
| .999998               | No entry | 
| .0000017             | Stop |
| .000000018         | Speed limit (20km/h) |
| .00000000042             | No passing |
| .000000000014           | Speed limit (30km/h) |
