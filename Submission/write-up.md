
# Project 2: Traffic Sign Classifier

## Initial approach and result
Probably like many students, I started out using the code from the LeNet lab. Initially I used my own CPU, it took an average 4 minutes, and the accuracy peaked at only **89%**, so I knew I had to do something different.

## GPU using AWS EC2
Following the instructions, I set up my GPU instance on AWS EC2 and immediately noticed the performance improvement, it took just a little under a minute to train my model

## Improving accuracy
I explored many different ways to improve my model's accuracy. Some of those ideas, e.g. applying dropout and adding more CNN layers, were based on my theoretical understanding of CNNs, but most of these were just trial and error, I didn't want to leave anything off the table and tried as many ways as I could to improve accuracy.

here is what I did sequentially and how they changed the accuracy:

#### 1) Changing sigma to 0.05, i.e. decreasing sigma by a half
This helped accuracy improve a little bit, it went up to **93.7%** by the 10th epoch.

#### 2) Preprocessing color images to grayscale
I converted the images to grayscale by averaging the RGB color channels for each pixel in each image. This took a lot of time and computing power, but the results barely changed, accuracy was still at **~93.5%**.

#### 3) Changing epoch from 10 to 20
This made the training process taking twice the amount of time, but accuracy did not improve much. Nevertheless, I decided to continue using `EPOCH=20` for all my subsequent configuration changes

#### 4) Adding a 1x1 CNN layer
I added this 1x1 CNN layer just after the first CNN layer. My hope was that if the result improved, then I would add more 1x1 CNN layers and make my model follow the inception module. Unfortunately the 1x1 CNN layer did not help, my accuracy dropped slightly to **92.7%**, so I did not add any more 1x1 CNN layer.

#### 5) Switching back to RGB
Up to this point, I'm still using the grayscale images preprocessed in step 2), I decided to switch back to RGB. As expected, this didn't change the accuracy much, which stayed at around **92.4%**.

#### 6) Decreasing batch size from 128 to 100
Since batch size may have some influence on the model, I decided to give it a shot. This took longer to train but the accuracy did improve a little, it reached **95.2%** at epoch 20.

#### 7) Applying a dropout/keep probability of 0.5
To apply dropout, I added a new `keep_prob` parameter to the `LeNet` function, so it became `LeNet(x, keep_prob)`. I applied the dropout at every CNN layer and the final fully connected layer. This unfortunately *decreased* the model's accuracy, to only **91.5%**. (I did make sure to use `keep_prob=1.0` for validation).

#### 8) Resetting batch size and removing 1x1 CNN layer
I decided to try tuning different parameters, so I reset the batch size and removed the 1x1 CNN layer. My accuracy went down to **89.6%**.

#### 9) Only apply dropout on the first CNN layer
Accuracy improved a little, to **93.3%**

#### 10) Reducing the learning rate from `0.001` to `0.0005`
Accuracy remained the same :(

#### 11) Increasing `keep_prob` to `.75` from `.5`
Accuracy again remained the same.

#### 12) Increasing depth layer of CNNs
This improved accuracy quite a lot, it went up to **95.0%**, I was quite happy with it at this point, but wanted to try a few more things.

#### 13) Apply dropout on the first two layers of CNNs
This again helped a bit, accuracy went up to **95.9%**.

#### 14) Apply `SAME` padding on the first CNN layer
This unfortunately decreased the accuracy slightly, down to **94.3%**, since the decrease is small, I decided to keep the `SAME` padding configuration.

#### 15) Apply dropout for all layers of CNNs
This gave me the best result yet :), accuracy improved to **96.1%**.

#### 16) Adding another CNN layer at the end of the second CNN layer, just before the fully connected layer
So now there are a total of 3 CNN layers. The last CNN layer took an input of size `6x6x22` to `4x4x50` and I did not apply the pooling layer at the end of the third CNN layer, because the height/width was already quite small at that point.

Since height and width were further reduced with the newly added CNN layer, I was initially not sure how much the depth should increase by, but later I found out that if the increase in depth is too small, then the parameters used in the fully connected layer will decrease, we don't want that so I made sure that the increase in depth either also increased the overall number of parameters or kept them the same.

This helped improve the accuracy to **96.5%**.

#### 17) Increasing batch size
In an effort to reduce training time (it was already taking about 2 minutes to train), I decided to increase the batch size to `200`. This unexpectedly also improved the accuracy to **97.2%**. I was quite happy with the accuracy so I decided to stop there.

## Running data against test set
Since I cannot use the test set multiple times, I decided to only use about 1/4 (3000 out of 12630) of the test set to test my model. I was quite happy that the accuracy on the 3000 test data set was **95.1%**.

## Use model to predict images found online
I did a simple Google search to find a list of 11 German traffic signs. Initially my model got 9 of 11 correct. However, the 2 that it did not correctly predict were signs that the training dataset didn't have a label for. So if I removed those, the accuracy reached 100% :). Also, judging by the softmax scores, the model was quite confident in its predictions.

## Ideas for further improvements
One potential improvement on the model is that some of the configuration changes did not result in accuracy improvement, and I did not reset these changes, the accuracy would likely improve if I had reset them.

Another potential improvement that I did not get to try was using average pooling instead of max pooling. Since the lecture told us that max pooling typically produces better results than average pooling, I did not use this configuration. Nevertheless, it may still help improve accuracy.
